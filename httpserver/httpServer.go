package httpserver

import (
	"log"
	"receiver-rest/controller"

	"github.com/gin-gonic/gin"
)

type HttpServer struct {
	router *gin.Engine
}

func InitHttpServer() HttpServer {
	messagesController := controller.MessagesController{}
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.POST("/api/messages", messagesController.Receive)

	return HttpServer{
		router,
	}
}

func (hs HttpServer) Start() {
	err := hs.router.Run("localhost:8080")
	if err != nil {
		log.Fatalf("HTTP Server starting error: %v", err)
	}
}
