# Receiver-REST



## Dependencies
Before starting the service, it is necessary to execute the following commands
to download the required dependencies:
```
go get -u github.com/gin-gonic/gin
```

## Running code

Code can be run with the following command:

```
go run main.go
```
