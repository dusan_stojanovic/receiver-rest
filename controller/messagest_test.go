package controller

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestReceive(t *testing.T) {
	router := initTestRouter()

	message := MessageDto{
		Content: "Hello World!!!",
	}
	body, _ := json.Marshal(message)

	request, _ := http.NewRequest("POST", "/api/messages", bytes.NewBuffer(body))
	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)
	assert.Equal(t, http.StatusOK, recorder.Result().StatusCode)
}

func initTestRouter() *gin.Engine {
	messagesController := MessagesController{}
	router := gin.Default()
	router.POST("/api/messages", messagesController.Receive)
	return router
}
