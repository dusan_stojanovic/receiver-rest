package controller

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type MessageDto struct {
	Content string `json:"content"`
}

type MessagesController struct{}

func (mc MessagesController) Receive(ctx *gin.Context) {
	body, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		log.Println("Server error: ", err)
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var message MessageDto
	err = json.Unmarshal(body, &message)
	if err != nil {
		log.Println("Server error: ", err)
		ctx.AbortWithError(http.StatusBadRequest, err)
	}

	fmt.Println(message.Content)
	ctx.Status(http.StatusOK)
}
