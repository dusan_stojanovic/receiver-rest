package main

import (
	"log"
	"receiver-rest/httpserver"
)

func main() {
	log.Println("Starting Receiver Service")
	httpServer := httpserver.InitHttpServer()
	httpServer.Start()
}
